import os
import pylick

__ROOT__    = os.path.dirname(pylick.__file__)

__table__   = __ROOT__ + '/data/index_table.dat'
__style__   = __ROOT__ + '/data/plotbelli.style'

__dirRes__  = '../outputs/'
__dirPlot__ = '../outputs/plots/'
