.. :changelog:

0.2.0 (2022-06-02)
++++++++++++++++++

- Code refactored.
- Integrate automated benchmark testcase from Borghi et al. (2022a) for next versions.
- Extend line lists to UV and NIR.
- New documentation with better examples and more discussion.

0.1.0 (2021-06-28)
++++++++++++++++++

- Initial release.