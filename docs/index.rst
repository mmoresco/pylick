.. pyLick documentation master file, created by Nicola Borghi on Tue Feb 2024.

.. raw:: html

    <style media="screen" type="text/css">
      h1 {display:none;}
    </style>

.. |br| raw:: html

    <div style="min-height:0.1em;"></div>

******
pyLick
******


.. raw:: html
   <br/>

   <div style="text-align:center;">    
   <img src="_static/spectrum_example.jpeg";" width="650"/>
   </div>

   <br/>

**pyLick** is a Python tool designed to measure spectral features, such as Lick indices and D4000, in galaxy spectra. It currently supports over 80 features spanning the near-UV to the near-IR. New ones can be easily introduced. The uncertainties are evaluated using the signal-to-noise method proposed by `Cardiel et al. (1998) <https://aas.aanda.org/articles/aas/abs/1998/03/ds1395/ds1395.html>`_. The code interpolates over bad pixels when a bad pixels mask is provided, allowing users to discard measurements above a specified Bad Pixel Fraction threshold. Additionally, the code includes convenient plotting routines.

.. image:: https://img.shields.io/badge/GitLab-mmoresco%2FpyLick-9e8ed7
    :target: https://gitlab.com/mmoresco/pylick/
.. image:: https://img.shields.io/badge/arXiv-2106.14894-28bceb
    :target: https://arxiv.org/abs/2106.14894
.. image:: https://readthedocs.org/projects/pylick/badge/?version=latest
    :target: https://pylick.readthedocs.io/en/latest/?badge=latest
.. image:: https://img.shields.io/badge/license-GPLv3-fb7e21
    :target: https://gitlab.com/mmoresco/pylick/-/blob/main/LICENSE
.. image:: https://img.shields.io/gitlab/v/release/14528131
    :target: https://gitlab.com/mmoresco/pylick/-/tags



Installation
------------

**pylick** can be installed using `Pypi <https://pypi.org/project/pylick>`_:

.. code-block:: bash

   pip install pylick

For more flexibility, clone the source repository into your working folder and install it locally:

.. code-block:: bash

   git clone https://gitlab.com/mmoresco/pylick.git
   cd pylick/
   pip install -e .

To test the installation, run the following command:

.. code-block:: bash

   python -c "import pylick; print(pylick.__version__)"


License & Attribution
---------------------

**pylick** is free software made available under the GPL-3 License. For details see the ``LICENSE``.

If you find this code useful in your research, please cite the following paper (`ADS <https://ui.adsabs.harvard.edu/abs/2022ApJ...927..164B/abstract>`_, `arXiv <https://arxiv.org/abs/2106.14894>`_, `INSPIRE <https://inspirehep.net/literature/1871797>`_):

.. code-block:: tex

    @ARTICLE{Borghi2022a,
        author = {{Borghi}, Nicola and {Moresco}, Michele and {Cimatti}, Andrea and et al.},
         title = "{Toward a Better Understanding of Cosmic Chronometers: Stellar Population Properties of Passive Galaxies at Intermediate Redshift}",
       journal = {ApJ},
          year = 2022,
         month = mar,
        volume = {927},
         pages = {164},
           doi = {10.3847/1538-4357/ac3240},
        eprint = {2106.14894},
        adsurl = {https://ui.adsabs.harvard.edu/abs/2022ApJ...927..164B},
    }


The team
--------

Main developers:

- `Michele Moresco (michele.moresco@unibo.it) <https://orcid.org/0000-0002-7616-7136>`_
- `Nicola Borghi (nicola.borghi6@unibo.it) <https://orcid.org/0000-0002-2889-8997>`_
- `Salvatore Quai (squai@uvic.ca) <https://orcid.org/0000-0002-0449-8163>`_

Contributors:
- `Alexandre Huchet <https://orcid.org/0000-0003-4756-341X>`_
- `Lucia Pozzetti <https://orcid.org/0000-0001-7085-0412>`_
- `Andrea Cimatti <https://orcid.org/0000-0002-4409-5633>`_


Documentation
-------------

.. toctree::
   :maxdepth: 2
   :caption: User Guide

   user_guide/Install
   user_guide/GettingStarted
   user_guide/Indices
   user_guide/SingleGalaxy
   user_guide/FullCatalog
   user_guide/Plotting
   user_guide/Citing



.. toctree::
   :maxdepth: 1
   :caption: Tutorials

   tutorials/quickstart


Changelog
---------

.. include:: changelog.rst


.. TO BULD THE DOCS
   python -m sphinx -T -E -b html -d _build/doctrees -D language=en . _build/html
   python -m sphinx -b latex -D language=en -d _build/doctrees . _build/latex
