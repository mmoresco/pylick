import numpy as np
from astropy.io import fits

from pylick.analysis import Galaxy


def load_spec(ID):
    def detect_spectrum_window(flux):
        """ Find the spectrum window excluding non positive side regions
        Input: 
            - flux 
        Output:
            - boolean mask with flux > 0 regions
        """
        il   = 0
        ir   = -1
        while True:
            if (flux[il] > 0):
                break
            il += 1
        while True:
            if (flux[ir] > 0):
                break
            ir -= 1        
        flag_window = np.ones_like(flux, dtype=bool)
        flag_window[:il] = 0
        flag_window[ir:] = 0
        return flag_window
    
    """ Loads spectroscopic data from file. """
    dir_spec = '/home/nic/cc/cc_legac/1_Catalog/legac_DR2_spectra/fits/'
    hdulist  = fits.open(dir_spec+'legac_'+ID.replace(" ", "")+'_v2.0.fits')

    wave = hdulist[1].data["WAVE"][0]
    flux = hdulist[1].data["FLUX"][0]
    ferr = hdulist[1].data["ERR"][0]
    qual = hdulist[1].data["QUAL"][0]

    mask = detect_spectrum_window(flux)
    
    spectrum = [wave[mask], 
                flux[mask], 
                ferr[mask],
                qual[mask]]
                
    hdulist.close()

    return spectrum



# IDs of the indices (24) to be measured
index_list = np.arange(22, 47)

ID = 'M1_206573'
wave, flux, err, qual = load_spec(ID)
z = 0.7018


settings = {'figsize' : (8,4.8),
            'xlab' : r'Restframe wavelength [$\AA$]',
            'ylab' : r'Flux [unitless]',
            'title' : ID,
            'outpath': '',
            'format' : 'pdf'
            }

ind = Galaxy(ID, index_list, spec_wave=wave, spec_flux=flux, spec_err=err, spec_mask=None,
				meas_method='int', z=z, plot=True,
                plot_settings=settings)

print(ind.vals)
print(ind.errs)