.. _Plotting:

Plotting
========


.. autofunction:: pylick.plot.spectrum_simple

.. autofunction:: pylick.plot.spectrum_with_indices


Additional settings
-------------------
Here below you will find the default `settings` dictionary which can be adapted for your needs. 
Other modifications can be made by accessing the `figure` object that is returned,
e.g., `fig = plot.spectrum_simple(...); fig.gca().set_xlim()`.

.. code-block:: python

    settings = {'figsize': (8,4.8),
                'xlab': r'Restframe wavelength [$\AA$]',
                'ylab': r'Flux [unitless]',
                # Colors of: flux, ferr, indices
                'spec_colors': ['#000080', '#8E8EC2', (.4,.4,.4,1)],
                # Style of the index patches (1: vband, 2: box)
                'ind_style': 1,
                # Fontsizes of: title, labels, indices 
                'spec_fontsizes': [14, 14, 13],
                'format': '.pdf',
                # Debug
                'inspect': False,
                # Filename.ext of the plot to be saved
                'filename': None,
               }
               