.. _GettingStarted:


Getting started
===============

pyLick is currently based on five modules: 

1. ``io`` implements the built-in methods for spectra I/O and preliminary analysis;  
2. ``indices`` loads the index library with pass-bands definitions; 
3. ``measurements`` derive multiple indices on one spectrum; 
4. ``analysis`` contains two main classes:

   a. *Galaxy*, optimized to analyze a single spectrum (wavelength, flux, flux error, [mask])
   b. *Catalog*, optimized to perform the analysis of a catalog of spectra

5. ``plot`` plotting routines.


The examples below show typical usage of pyLick.


Workflow 1
----------
(1) Import a **.fits** spectrum, (2) load the table of spectral indices to measure, (3) call the *Galaxy* class,

.. code-block:: python

   import pylick.io as io
   from pylick.indices import IndexLibrary
   from pylick.analysis import Galaxy

   spectrum     = io.load_spec_fits(dir_spec, filename, colnames=['lambda', 'flux', 'flux_err'])
   ind_library  = IndexLibrary(index_keys)

   ind_measured = Galaxy(ID, index_list, spec_wave=spectrum[0], spec_flux=spectrum[1], spec_err=spectrum[2], z=z)
   vals, errs   = ind_measured.vals, ind_measured.errs
   print(ind_measured)


Workflow 2
----------
Define a function to load the spectra from a catalog folder, (2) load the table of spectral indices to measure, (3) call the *Catalog* class,

.. code-block:: python

   import pylick.io as io
   from pylick.indices import IndexLibrary
   from pylick.analysis import Catalog

   def load_spec(ID):
      ...
      return [wave, flux, ferr, mask]

   IDs = [...]
   ind_library  = IndexLibrary(index_keys)

   ind_measured = Catalog(IDs, load_spec, index_keys, z=zs, do_plot=True, verbose=True)

Workflow 3
----------
Call the *MeasSpectrum* class defined in ``pylick.measurement`` to verify intermediate measurements for debug purposes.
