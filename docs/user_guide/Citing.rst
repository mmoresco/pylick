.. _Citing:

Citing pyLick
=============

If you find this code useful in your research, please cite the following paper (`ADS <https://ui.adsabs.harvard.edu/abs/2022ApJ...927..164B/abstract>`_, `arXiv <https://arxiv.org/abs/2106.14894>`_, `INSPIRE <https://inspirehep.net/literature/1871797>`_):


.. code-block:: tex

    @ARTICLE{Borghi2022a,
        author = {{Borghi}, Nicola and {Moresco}, Michele and {Cimatti}, Andrea and et al.},
         title = "{Toward a Better Understanding of Cosmic Chronometers: Stellar Population Properties of Passive Galaxies at Intermediate Redshift}",
       journal = {ApJ},
          year = 2022,
         month = mar,
        volume = {927},
         pages = {164},
           doi = {10.3847/1538-4357/ac3240},
        eprint = {2106.14894},
        adsurl = {https://ui.adsabs.harvard.edu/abs/2022ApJ...927..164B},
    }


Papers that use pyLick
----------------------

- Borghi et al. (2022a) - *Toward a Better Understanding of Cosmic Chronometers: Stellar Population Properties of Passive Galaxies at Intermediate Redshift*, ApJ 927 22 (`ADS <https://ui.adsabs.harvard.edu/abs/2022ApJ...927..164B/abstract>`_)
- Borghi et al. (2022b) - *Toward a Better Understanding of Cosmic Chronometers: A New Measurement of H(z) at z~0.7*, ApJL 928 L4 (`ADS <https://ui.adsabs.harvard.edu/abs/2022ApJ...928L...4B/abstract>`_)
- Talia et al. (2023) - *The VANDELS ESO public spectroscopic survey: The spectroscopic measurements catalogue*, A&A 678 25 (`ADS <https://ui.adsabs.harvard.edu/abs/2023A%26A...678A..25T/abstract>`_)

If you have a paper that you would like to add to the list, please contact us.
