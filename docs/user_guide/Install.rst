.. _Install:

Installation
============

pyLick is a pure Python (>3) code with only four dependencies: `numpy <http://www.numpy.org/>`_ (math operations), `scipy <https://www.scipy.org/>`_ (interpolation functions), `matplotlib <https://matplotlib.org/>`_ (plotting), `astropy <https://www.astropy.org/>`_ (I/O operations), which are automatically installed.


Using pip
---------
pyLick can be easily and quicly installed just by using `Pypi <https://pypi.org/project/pylick>`_:

.. code-block:: bash

   pip install pylick


From source
-----------
If you want to modify the code you can clone the source repository hosted on on GitLab

.. code-block:: bash

    python -m pip install -U pip
    python -m pip install -U setuptools setuptools_scm pep517
    git clone https://gitlab.com/mmoresco/pylick.git
    cd pylick
    python -m pip install -e .


Issues
------

If you find issues in the installation process please contact nicola.borghi6@unibo.it. The code has been tested on Linux (Ubuntu 20.04 and Debian GNU/Linux 11).
