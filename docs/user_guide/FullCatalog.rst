.. _FullCatalog:

Full catalog analysis
=====================

.. autoclass:: pylick.analysis.Catalog
   :inherited-members:
