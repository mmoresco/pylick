.. _SingleGalaxy:

Single galaxy analysis
======================

.. autoclass:: pylick.analysis.Galaxy
   :inherited-members:
