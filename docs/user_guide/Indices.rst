.. _SpectralIndices:

Indices
=======

Definitions
-----------

**pylick** handles different indices definitions depending on the `units` value in the index table.

* `A` (atomic Lick index)

.. math::

   I_a(\text{[Å]}) =\int_{\lambda_{c_1}}^{\lambda_{c_2}} \left( 1-\frac{F(\lambda)}{F_c(\lambda)}\right)\;\mathrm{d}{\lambda}


* `mag` (molecular Lick index)

.. math::

   I_m([\mathrm{mag}])=-2.5\log \left( \frac{1}{\lambda_{c_2}-\lambda_{c_1}}\int_{\lambda_{c_1}}^{\lambda_{c_2}}\frac{F(\lambda)}{F_c(\lambda)}\;\mathrm{d}{\lambda}\right)

* `break_nu` (e.g., D4000-type),

.. math::

    \mathrm{D4000}=\frac{\lambda_{b2}-\lambda_{b1}}{\lambda_{r2}-\lambda_{r1}} \frac{ \int_{\lambda_{r1}}^{\lambda_{r2}}\lambda^2 F(\lambda) \;\mathrm{d}{\lambda}} { \int_{\lambda_{b1}}^{\lambda_{b2}} \lambda^2 F(\lambda)\;\mathrm{d}{\lambda}}

* `break_lb` (e.g., B4000-type),

.. math::

    \mathrm{B4000}=\frac{\lambda_{b2}-\lambda_{b1}}{\lambda_{r2}-\lambda_{r1}} \frac{ \int_{\lambda_{r1}}^{\lambda_{r2}} F(\lambda) \;\mathrm{d}{\lambda}} { \int_{\lambda_{b1}}^{\lambda_{b2}} F(\lambda)\;\mathrm{d}{\lambda}}

* `bump` (e.g., MgUV-type),

.. math::

    \rm{Mg_{UV}} = \frac{2\int^{2725}_{2625}F(\lambda)\rm{d}\lambda}{\int^{2625}_{2525}F(\lambda)\rm{d}\lambda+\int^{2825}_{2725}F(\lambda)\rm{d}\lambda},


where :math:`F(\lambda)` and :math:`F_c(\lambda)` are the spectrum flux and the local pseudo-continuum, respectively. 
The latter is derived through linear interpolation: :math:`F_c(\lambda) = F_b \frac{\lambda_r -\lambda}{\lambda_r -\lambda_b} + F_r \frac{\lambda -\lambda_b}{\lambda_r -\lambda_b}`, 
where :math:`\lambda_{b,r}` and :math:`F_{b,r}` are the central wavelengths and mean fluxes of the lateral blue and red regions.



Measuring methods
-----------------

**pylick** handles different measuring methods. Basically, this is to account for the fact that the pixels of the observed spectrum may cover the full index region only partially.

* `int` (Default): the spectrum is interpolated with a fine bin size of :math:`\text{Å}`;
* `exact`: the spectrum is interpolated with scipy.interpolate.interp1d (0th order). The resulting function can be evaluated at each wavelenght, therefore the measurement is more precise at the edges of pass-bands windows;
* `wei`: the flux of pixels is weighted by the fraction covered by the pass-bands definition.

performances (*estimates for a 3 GHz processor*, TBchecked)

* `int`: 0.005 s/index/galaxy
* `wei`: 0.010 s/index/galaxy
* `exact``: 0.125 s/index/galaxy




Index library
-------------
Standard usage of ``pylick`` involves instantiating a :class:`pylick.analysis.IndexLibrary`. 


.. autoclass:: pylick.indices.IndexLibrary
   :inherited-members:



Available indices
-----------------
*Last update 01 jun 2022*

.. include:: ../../pylick/data/index_table.dat
   :literal:
